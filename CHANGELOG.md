# Changelog

## 0.1.2 (2018-05-07)

* Haxe 4 compatibility

## 0.1.1 (2017-12-23)

* Moved ShouldClassNameDef to avoid buddy dependency outside tests

## 0.1.0 (2017-11-18)

* Initial release
